var leanPlumUrl = "http://www.leanplum.com/api";

var appId = "et9EQFFCDv2Mnm8QgvCcQHGhQCqE8KroQyQ7nVMgUw4";
var productionId = "caAFkt4tpmMZN3ZXhlxtu9obIFfuvY6cx2zKrL4h9LM";
var developmentId = "LBkkLMZo6Z4zG51bXjGxXjkZnFg1ws2aIdV9Micdvsg";

var args = {
	"appId": appId,
	"clientKey":productionId,
//	"devMode": true,
	"deviceId": Ti.Platform.id,
	"userId" : "",
	"apiVersion": "1.0.6"
};

function start(_cb){
	var xhr = Ti.Network.createHTTPClient(); 
	xhr.onload = function(e) { _cb(JSON.parse(this.responseText));};
	xhr.onerror = function(e) {_cb(null);};
	xhr.open( "POST", leanPlumUrl);
	var startArgs = args;
	startArgs.action = "start";	
	xhr.send(startArgs);
}
function stop(){
	var xhr = Ti.Network.createHTTPClient(); 
	xhr.onload = function(e) {};
	xhr.open( "POST", leanPlumUrl);
	var stopArgs = args;
	stopArgs.action = "stop";	
	xhr.send(stopArgs);
}

exports.start = start;
exports.stop = stop;