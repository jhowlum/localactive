/**
 * @author James How Lum
 */

// Miscellaneous utility functions

// For DEMO only
var offsetSeconds = (20+Math.round(Math.random()*20))*60; //0;
var offsetThreshold = 5;

function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}

function calculateDuration(start, stop){
	var seconds = Math.floor((stop - start)/1000);
	
	// For DEMO only
	if (seconds > offsetThreshold) seconds = seconds + offsetSeconds;
	
	var labelHour = Math.floor(seconds/3600);
	var labelMin = Math.floor(seconds/60 - labelHour*60);
	var labelSec = seconds - labelHour * 3600 - labelMin * 60;
	return {
		hour: labelHour,
		min: labelMin,
		sec: labelSec
	};	
}

exports.leftPad = leftPad;
exports.calculateDuration = calculateDuration;
