// NOTE: The below is only for demonstration purposes. Disable for production.

Ti.API.info('seeded: ' + Ti.App.Properties.hasProperty('seeded'));
//determine if the database needs to be seeded
if (!Ti.App.Properties.hasProperty('seeded')) {

	var activities = ["Walking", "Cycling", "Parks & playgrounds"];
	
	var models = [];
	for(var j = 0; j < 7; ++j){
		var now = new Date();
		var stopTime = new Date(now.getTime() - (Math.random()*6+parseInt(1))*1000*60*60*24);
		
		var stopString = 
			stopTime.getHours() + ":" +
			stopTime.getMinutes() + ", " +
			stopTime.toLocaleDateString();
		models.push({
        	"name" : activities[parseInt(Math.floor(Math.random()*3))],
        	"durationString" : Math.round(40 - Math.random()*20)+"min",
        	"stopTime" : stopTime.getTime(),
        	"stopTimeString" : stopString
   		});
	}
    // add all items to collection
    Alloy.Collections.sessions.reset(models);

    // save all of the elements
    Alloy.Collections.sessions.each(function(_m) {
        _m.save();
    });

	Alloy.Collections.sessions.fetch();
	
    Ti.App.Properties.setString('seeded', 'yuppers');

    $.tabGroup.open();

} else {

    $.tabGroup.open();

}

// force tables to update
Alloy.Collections.sessions.fetch();
