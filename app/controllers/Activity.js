Alloy.CFG = {"activityTab":$.activityTab};

var GA = require("analytics.google");
var tracker = GA.getTracker("UA-43842743-1");

function doClick(buttonItem){
	var typeString = buttonItem.source.title;
	tracker.trackEvent({
		category: "Activity button",
		action: "clicked",
		label: typeString,
		value: 1
	});
	tracker.trackScreen("startMap");
	//console.log('type is ' + typeString);
	var startMapController = Alloy.createController('StartMap', {
		activityType: typeString
	});
	$.activityTab.open(startMapController.getView());	
}

$.walkingButton.addEventListener('click', doClick);
$.cyclingButton.addEventListener('click', doClick);
$.parksButton.addEventListener('click', doClick);

if (Ti.Platform.osname === 'iphone') {
    $.login.style = Titanium.UI.iPhone.SystemButtonStyle.PLAIN;
    $.activityWindow.setRightNavButton($.login);
}

function updateActivityLabel() {
	var today = new Date();
	if (Ti.App.Properties.hasProperty("today") &&
		Ti.App.Properties.getString("today") === today.toLocaleDateString() ){
		var duration = Ti.App.Properties.getInt("duration", 0);
		$.summaryLabel.text = "Activity today: " + duration + " minutes";
	}	
}

// Calculate the current activity for today
Ti.App.addEventListener('updateDuration', updateActivityLabel);

updateActivityLabel();
