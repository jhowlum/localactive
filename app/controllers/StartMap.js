var args = arguments[0] || {};

var Map = require('ti.map');
var Utility = require('utility');

// CartoDB jhowlum API key
var key = "71188d073aa909b28d521caccc18a4fa73b5a0d6";
// Converted shapefiles with ogr2ogr then zipped up output files and uploaded to CartoDB: 
// > ogr2ogr -f "ESRI Shapefile" Playground.shp ~/Downloads/parklands-bike-ped-path/Parklands\ bike_ped\ path.sh -t_srs "EPSG:4326"

var startTitle = "Start my activity timer";
var stopTitle = "I've completed my activity";

function enableStart(){
	$.startButton.enabled = true;
	$.startButton.title = startTitle;
}

function getPosition(callback){
	var result = {
		latitude: null,
		longitude: null
	};
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (!e.error) {
			result.latitude = e.coords.latitude;
			result.longitude = e.coords.longitude;
		} else {
			Ti.UI.createAlertDialog({
        		title : "Error",
            	message : "Sorry, couldn't determine your location. Try selecting your activity again."
        	}).show();
        	return result;
		}
		callback(result);
	});
}

// TODO: factor out the formatting functions
function createParkAnnotations(parkList){
	if (parkList.length === 0) {
		alert('Sorry, there are no parks in your local area.');
		return;
	}
	if (parkList.length === 0) return;
	var count = parkList.length;
	for(var i = 0; i < count; ++i){
		var sub = "";
		if (parkList[i].properties.drink > 0){
			sub = "Water ";
		}
		if (parkList[i].properties.gym === "Yes"){
			sub = sub + "Gym ";
		}
		if (parkList[i].properties.equip > 0){
			sub = sub + "Play-equipment ";
		}
		if (parkList[i].properties.toilet === "Yes"){
			sub = sub + "Toilet ";
		}
		var ann = Map.createAnnotation({
			latitude : parkList[i].geometry.coordinates[1],
			longitude : parkList[i].geometry.coordinates[0],
			title : "Park: " + parkList[i].properties.name,
			subtitle : sub,
			pincolor : Map.ANNOTATION_GREEN,
			animate : true
		}); 
		$.mapView.addAnnotation(ann);
	}
}

// TODO: factor out the spatial database requests
function annotatePlaygrounds(region){
	var baseURL = "http://jhowlum.cartodb.com/api/v2/sql?";
	console.log(JSON.stringify(region));
	var regionString = 
		"ST_MakeBox2D(ST_Point(" + region.xmin + "," + region.ymin + "),ST_Point(" + region.xmax + "," + region.ymax + "))";
	var query = "SELECT NAME, DRINK, TOILET, GYM, EQUIP, the_geom FROM playground WHERE the_geom && " + regionString;
    var postData = {
    	q: query,
    	key: key,
    	format: "GeoJSON"
	};
	
	var xhr = Ti.Network.createHTTPClient({
  		onload: function(e) {
//            Ti.API.info(this.responseText);
//            alert('success on playgrounds');
 
            var response = JSON.parse(this.responseText);
            response = response.features;
 
            Ti.API.info("Number of parks" + response.length);
            Ti.API.info(response[1]);
            createParkAnnotations(response);
            enableStart();
        },        
        onerror: function(e) {
            Ti.API.info(this.responseText);
            alert("Couldn't fetch playgrounds");
            enableStart();
        },
        timeout:10000  // in milliseconds 
    });
 
    xhr.open("POST", baseURL);
  
    xhr.send(postData);
}

function createBikeDirectRoutes(routeList){
	if (routeList.length === 0) {
		alert('Sorry, there are no routes in your local area.');
		return;
	}
	var count = routeList.length;
	for(var i = 0; i < count; ++i){
		var route = routeList[i].geometry.coordinates[0];
		
		console.log("BD Route: " + route[0]);
		if (routeList[i].properties.rdname &&
			routeList[i].properties.rdname != "undefined" &&
			routeList[i].properties.rdname != "" &&
			routeList[i].properties.rdname != " ") {
			var ann = Map.createAnnotation({
				latitude : route[0][1],
				longitude : route[0][0],
				title : "Route: " + routeList[i].properties.rdname,
				subtitle : routeList[i].properties.type,
				pincolor : Map.ANNOTATION_RED,
				animate : true
			}); 
			$.mapView.addAnnotation(ann);
		}
		var path = [];
		for(var j = 0; j < route.length; ++j){
			var point = {
				latitude: route[j][1],
				longitude: route[j][0]
			};
			path.push(point);
		}
		var mapRoute = Map.createRoute({
			//name: "Route: " + routeList[i].properties.rdname,
			color: "red",
			points: path,
			width: 4
		});
		$.mapView.addRoute(mapRoute);
	}
}

function annotateBikeDirectRoutes(region){
	var baseURL = "http://jhowlum.cartodb.com/api/v2/sql?";
	console.log(JSON.stringify(region));
	var regionString = 
		"ST_MakeBox2D(ST_Point(" + region.xmin + "," + region.ymin + "),ST_Point(" + region.xmax + "," + region.ymax + "))";
	var query = "SELECT RDNAME, TYPE, the_geom FROM offroadpath WHERE the_geom && " + regionString;
    var postData = {
    	q: query,
    	key: key,
    	format: "GeoJSON"
	};
	
	var xhr = Ti.Network.createHTTPClient({
  		onload: function(e) {
            Ti.API.info(this.responseText);
            var response = JSON.parse(this.responseText);
            response = response.features;
 
            Ti.API.info("Number of bike routes " + response.length);
            Ti.API.info(response[1]);
            createBikeDirectRoutes(response);
            enableStart();
        },        
        onerror: function(e) {
            Ti.API.info(this.responseText);
            alert("Couldn't fetch BikeDirect");
            enableStart();
        },
        timeout:10000  // in milliseconds 
    });
 
    xhr.open("POST", baseURL);
  
    xhr.send(postData);
	
}

function createParklandsPaths(routeList){
	if (routeList.length === 0) {
		alert('Sorry, there are no paths in your local area.');
		return;
	}
	var count = routeList.length;
	for(var i = 0; i < count; ++i){
		var route = routeList[i].geometry.coordinates[0];

		console.log("PP Route: " + route[0]);
		if (routeList[i].properties.path_type &&
			routeList[i].properties.path_type != "undefined" &&
			routeList[i].properties.path_type != "") {
			var ann = Map.createAnnotation({
				latitude : route[0][1],
				longitude : route[0][0],
				title : "Route: " + routeList[i].properties.path_type,
				pincolor : Map.ANNOTATION_VIOLET,
				animate : true
			}); 
			$.mapView.addAnnotation(ann);
		}
		var path = [];
		for(var j = 0; j < route.length; ++j){
			var point = {
				latitude: route[j][1],
				longitude: route[j][0]
			};
			path.push(point);
		}
		var mapRoute = Map.createRoute({
			//name: "Route: " + routeList[i].properties.path_type,
			color: "purple",
			points: path,
			width: 4
		});
		$.mapView.addRoute(mapRoute);
	}
}
function annotateParklandsPaths(region){
	var baseURL = "http://jhowlum.cartodb.com/api/v2/sql?";
	console.log(JSON.stringify(region));
	var regionString = 
		"ST_MakeBox2D(ST_Point(" + region.xmin + "," + region.ymin + "),ST_Point(" + region.xmax + "," + region.ymax + "))";
	var query = "SELECT PATH_TYPE, the_geom FROM parklandspaths WHERE the_geom && " + regionString;
    var postData = {
    	q: query,
    	key: key,
    	format: "GeoJSON"
	};
	
	var xhr = Ti.Network.createHTTPClient({
  		onload: function(e) {
//            Ti.API.info(this.responseText);
//            alert('success on parklands');
 
            var response = JSON.parse(this.responseText);
            response = response.features;
 
            Ti.API.info("Number of bike routes " + response.length);
            Ti.API.info(response[0]);
            createParklandsPaths(response);
            enableStart();
        },        
        onerror: function(e) {
            Ti.API.info(this.responseText);
            alert("Couldn't fetch parklands paths");
            enableStart();
        },
        timeout:10000  // in milliseconds 
    });
 
    xhr.open("POST", baseURL);
  
    xhr.send(postData);
	
}


function setMapRegion(position) {
	if (position.latitude == null || position.longitude == null){
		$.startMapWindow.close();
	}
	
	$.mapView.setRegion({
		latitude : position.latitude,
		longitude : position.longitude,
		latitudeDelta : 0.011,
		longitudeDelta : 0.011
//		latitudeDelta : 10.02,
//		longitudeDelta : 10.02
	});
	
	var region = $.mapView.region;
	var extents = {
		xmin: region.longitude-region.longitudeDelta,
		ymin: region.latitude-region.latitudeDelta,
		xmax: parseFloat(region.longitude)+parseFloat(region.longitudeDelta),
		ymax: parseFloat(region.latitude)+parseFloat(region.latitudeDelta),
	};
	// Fetch the data relevant data from the online spatial database
	if (args.activityType === "Parks & playgrounds"){
		// Get playground data as annotation
		annotatePlaygrounds(extents);
	} else {
		// Get bike direct data
		annotateBikeDirectRoutes(extents);
		
		// Get parklands path data
		annotateParklandsPaths(extents);
	}

}

$.startMapWindow.title = args.activityType;

var startTime;
var timer;

function stopClicked(){
	clearInterval(timer);
	
	var stopTime = new Date();
	console.log('Stop time: ' + stopTime.toLocaleTimeString());
	
	// Calculuate duration
	var duration = Utility.calculateDuration(startTime, stopTime);
	var minutes = duration.min + duration.hour * 60;
	var seconds = duration.sec;

	// Add new session
	var stopString = 
		stopTime.getHours() + ":" +
		stopTime.getMinutes() + ", " +
		stopTime.toLocaleDateString();
	var sessionModel = Alloy.createModel("sessions", {
		name: args.activityType,
 		startTime: startTime.getTime(),
 		stopTime: stopTime.getTime(),
		stopTimeString: stopString,
		duration: (minutes*60 + seconds),
		durationString: minutes + "min"//+leftPad(seconds,2)+"s"
	});
//	Alloy.Collections.sessions.add(sessionModel);
	sessionModel.save();
	Alloy.Collections.sessions.fetch();
	
	// Set today's total
	var duration = minutes;
	var today = new Date();
	if (Ti.App.Properties.hasProperty("today") &&
		Ti.App.Properties.getString("today") === today.toLocaleDateString() ){
		duration = duration + Ti.App.Properties.getInt("duration", 0);
	} else {
		Ti.App.Properties.setString('today', today.toLocaleDateString());
	}
	Ti.App.Properties.setInt('duration', duration);
	Ti.App.fireEvent("updateDuration");
	
	var leanplum = require('leanplum');
	
	leanplum.start(function(data){
		var minMinutes = 30;	
		var successText = "Excellent! You've done your %d minutes of moderate intensity exercise today!";
		console.log( "data: " + JSON.stringify(data));
		if (data != null && data.response[0].success != 0) {
			var vars = data.response[0].vars;
			if (vars.successText) successText = vars.successText;
			if (vars.minMinutes) minMinutes = vars.minMinutes;
		}
		var trailingText = 
			String.format("You need to do %d more minutes to complete your %d minutes of moderate intensity exercise today.",
			minMinutes-duration, minMinutes);
		if (duration >= minMinutes) trailingText = String.format(successText, minMinutes);
		var activityVerb = "were active in the park";
		if (args.activityType === 'Walking') activityVerb = "walked";
		if (args.activityType === 'Cycling') activityVerb = "cycled";
		var messageText = 
			'You ' + activityVerb + ' for\n' + minutes + " minutes.\n" + trailingText;
		Ti.UI.createAlertDialog({
       		title : "Great job!",
       		message : messageText
   		}).show();
		
		if (Ti.Platform.osname == 'android') {
			setTimeout(function() {
 	   			$.startMapWindow.close();
			},2000);
		} else {
 	   		$.startMapWindow.close();
		}
	});
	leanplum.stop();
}

function startClicked(){
	$.startButton.title = stopTitle;
	startTime = new Date();
	timer = setInterval(function(){
		var current = new Date();
		var duration = Utility.calculateDuration(startTime, current);
  	  	$.timerLabel.text = 
  	  		Utility.leftPad(duration.hour, 2) + ":" + Utility.leftPad(duration.min, 2) + ":" + Utility.leftPad(duration.sec, 2);
	}, 1000);
}

$.startButton.addEventListener('click', function(){
	if ($.startButton.title === startTitle){
		startClicked();
	} else {
		stopClicked();
	}
});

// Enable geolocation
Ti.Geolocation.purpose = 'Showing you activity spaces in your local community';
if (Ti.Geolocation.locationServicesEnabled) {
	Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
} else {
	Ti.UI.createAlertDialog({
        title : "Error",
        message : 'Sorry, I need your location. Please enable geolocation in your settings.'
    }).show();
    $.startMap.close();
}

$.startButton.enabled = false;
$.startButton.title = 'Searching in your local area...';

// Initialise the map
getPosition(setMapRegion);

// Optional: add event listener for when the map changes and destroy and request geoinformation

