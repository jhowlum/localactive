exports.definition = {
	config: {
		columns: {
		    "name": "text",
		    "startTime": "integer", //milliseconds since epoch
		    "stopTime": "integer",
		    "stopTimeString": "text", // human-readable date and time
		    "duration": "integer", // seconds
		    "durationString": "text",
		    "startLat": "real",
		    "startLong": "real",
		    "stopLat": "real",
		    "stopLong": "real"
		},
		defaults: {
		    "name": "",
		    "startTime": 0,
		    "stopTime": 0,
		    "stopTimeString": "",
		    "duration": 0, 
		    "durationString": "",
		    "startLat": 0,
		    "startLong": 0,
		    "stopLat": 0,
		    "stopLong": 0
		},
		adapter: {
			type: "sql",
			collection_name: "sessions"
		}
	},		
	extendModel: function(Model) {		
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});
		
		return Model;
	},
	extendCollection: function(Collection) {		
		_.extend(Collection.prototype, {
			// extended functions and properties go here
            // Implement the comparator method.
    	    comparator : function(item) {
        	    return -item.get('stopTime');
            }
		});
		
		return Collection;
	}
}

