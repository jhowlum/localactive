/**
 * @author James How Lum
 */

// Test the Activity screen

describe("The Activity screen", function() {
	var Alloy = require("alloy");
	var $;
	
	beforeEach(function() {
		$ = Alloy.createController("Activity");
	});
	
	// Test UI look
	it("contains the right buttons", function() {
		expect($.__views.walkingButton.title).toEqual("Walking");
		expect($.__views.cyclingButton.title).toEqual("Cycling");
		expect($.__views.parksButton.title).toEqual("Parks & playgrounds");
	});
	
	// Test the update label.
	describe("has an activity label", function(){
		beforeEach(function() {
			// Reset the activity label
			$.__views.summaryLabel.text = "Activity today: 0 minutes";
		});
		
		afterEach(function() {
			// Remove any applied properties
			Ti.App.Properties.removeProperty("today");
			Ti.App.Properties.removeProperty("duration");
		});
		
		// Test the default value
		it("with a default value", function() {
			expect($.__views.summaryLabel.text).toEqual("Activity today: 0 minutes");
		});
		
		// Test the update event.
		it("which updates on an event", function() {
			var today = new Date();
			Ti.App.Properties.setString('today', today.toLocaleDateString());
			Ti.App.Properties.setInt('duration', 5);
			Ti.App.fireEvent("updateDuration");
			waits(500);
			runs(function() {
				expect($.__views.summaryLabel.text).toEqual("Activity today: 5 minutes");		
			});
		});
	});
});
