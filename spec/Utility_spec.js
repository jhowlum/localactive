/**
 * @author James How Lum
 */

describe("The Utility lib", function() {
	var Utility = require('utility'); 

	it("has a leftPad method", function() {
		expect(Utility.leftPad(5, 2)).toEqual("05");
		expect(Utility.leftPad(25, 1)).toEqual("25");
		expect(Utility.leftPad(25, 2)).toEqual("25");
		expect(Utility.leftPad(25, 3)).toEqual("025");
	});
	
	it("has a calculateDuration method", function() {
		var duration = Utility.calculateDuration(1000, 4000);
		var expected = {
			hour: 0,
			min: 0,
			sec: 3
		};
		expect(duration).toEqual(expected);
	});
});
